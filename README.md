# Good tools in home bin


## Tools list

* fs - combine tool of rg and fzf
* fspreview - previewer of fs
* arg - wrapped of ag or rg search tool
* ff - find a file with fzf selecting
* vd - find diff files in two pathes and query user to open the diff files with nvim
* c - compile c file or project, then run the compiled elf if it can be executed
